// Diver program that runs the matrix multiplication algorithms

#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include "matrix.hpp"

// fails if the last number has more than one digit
void createMatrix(std::ifstream &matrixStream, Matrix &matrix){
    if(matrixStream.is_open()){
	unsigned long int i = 0;
	unsigned long int j = 0;
	unsigned long int size;
	std::string line;
	std::string number;
	std::getline(matrixStream, number);
	size = std::stoi(number);
	matrix.setSize(size);
	number = "";
	while(std::getline(matrixStream, line)){
	    for(auto &c : line){
		if(!isspace(c)){
		    number += c;
		}else{
		    matrix.setNum(i, j, std::stoi(number));
		    j++;
		    number = "";
		}
	    }
	    j = 0;
	    i++;
	}
	matrixStream.close();
    }else{
	std::cerr << "Error! Unable to read the files.\n";
	return;
    }
}

int main(int argc, char *argv[]){
    if(argc != 3){
	std::cerr << "Usage: " << argv[0] << " Matrix0 Matrix1" << std::endl;
	return 1;
    }
    std::ifstream matrixStream0(argv[1]);
    std::ifstream matrixStream1(argv[2]);

    Matrix matrix0("Matrix0");
    Matrix matrix1("Matrix1");

    std::cout << "Creating matrix0\n";
    createMatrix(matrixStream0, matrix0);
    std::cout << "Creating matrix1\n";
    createMatrix(matrixStream1, matrix1);    

    std::cout << matrix0;
    std::cout << matrix1;
    std::cout << "=============================================================\n";

    std::cout << "Adding matrices using overloaded operator\n";
    Matrix result = matrix1 + matrix0;
    std::cout << result;

    std::cout << "Naive multiply\n";
    result = matrix0.naiveMultiply(matrix1);
    std::cout << result;
    std::cout << "=============================================================\n";

    std::cout << "Testing getSubMatrix(1)\n";
    Matrix subMatrix0 = matrix0.getSubMatrix(1);
    std::cout << subMatrix0;
    std::cout << "Testing getSubMatrix(2)\n";
    Matrix subMatrix1 = matrix0.getSubMatrix(2);
    std::cout << subMatrix1;
    std::cout << "Testing getSubMatrix(3)\n";
    Matrix subMatrix2 = matrix0.getSubMatrix(3);
    std::cout << subMatrix2;
    std::cout << "Testing getSubMatrix(4)\n";
    Matrix subMatrix3 = matrix0.getSubMatrix(4);
    std::cout << subMatrix3;
    std::cout << "=============================================================\n";

    std::cout << "Testing joinMatrices\n";
    Matrix superMatrix("Supermatrix", 0);
    superMatrix.joinMatrices(subMatrix0, subMatrix1, subMatrix2, subMatrix3);
    std::cout << superMatrix;
    std::cout << "=============================================================\n";

    result = matrix0.strassenMultiply(matrix1);
    std::cout << result;
    return 0;
}

