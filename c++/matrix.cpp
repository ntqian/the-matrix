#include "matrix.hpp"
#include <omp.h>

Matrix::Matrix(std::string name, unsigned long int size){
    Matrix::name = name;
    Matrix::size = size;
    matrix.resize(size);
    for(auto &row : matrix){
	row.resize(size);
    }
}

Matrix::Matrix(std::string name){
    Matrix::name = name;
    size = 0;
}

void Matrix::setSize(unsigned long int n){
    size = n;
    matrix.resize(n);
    for(auto &row : matrix){
	row.resize(n);
    }
}

void Matrix::setNum(unsigned long int i, unsigned long int j, int num){
    if(i >= size || j >= size){
	std::cerr << "Index out of bounds error!\n";
	return;
    }
    matrix[i][j] = num;
}

int Matrix::getNum(unsigned long int i, unsigned long int j) const{
    if(i >= size || j >= size){
	std::cerr << "Index out of bounds error!\n";
    }
    return matrix[i][j];
}

// fails with matrices size of 4
Matrix Matrix::getSubMatrix(int quadrant){
    if(quadrant < 1 || quadrant > 4){
	std::cerr << "Quandrant ranges only between 1 to 4\n";
    }
    unsigned long int xoffset = 0;
    unsigned long int yoffset = 0;
    if(quadrant == 3 || quadrant == 4){
	xoffset = size/2;
    }
    if(quadrant == 2 || quadrant == 4){
	yoffset = size/2;
    }
    Matrix subMatrix("Submatrix", size/2);
    for(unsigned long int i = 0 + xoffset; i < size/2 + xoffset; i++){
	for(unsigned long int j = 0 + yoffset; j < size/2 + yoffset; j++){
	    int num = matrix[i][j];
	    subMatrix.matrix[i - xoffset][j - yoffset] = num;
	}
    }
    return subMatrix;
}

// initial suboptimal implemenation
// current version may ecounter cache misses since its iterating row by row in the supermatrix
void Matrix::joinMatrices(const Matrix &m1, const Matrix &m2, const Matrix &m3, const Matrix &m4){
    if(m1.size >= size){
	setSize(2 * m1.size);
    }
    unsigned long int offset = m1.size;
    int num;
    for(unsigned long int i = 0; i < size; i++){
	for(unsigned long int j = 0; j < size; j++){
	    if(i < offset && j < offset){
		//std::cout << "Retrieving from m1\n";
		num = m1.matrix[i][j];
	    }else if(i < offset && j >= offset){
		//std::cout << "Retrieving from m2\n";
		num = m2.matrix[i][j - offset];
	    }else if(i >= offset && j < offset){
		//std::cout << "Retrieving from m3\n";
		num = m3.matrix[i - offset][j];
	    }else if(i >= offset && j >= offset){
		//std::cout << "Retrieving from m4\n";
		num = m4.matrix[i - offset][j - offset];
	    }
	    //std::cout << "Inserting " << num << " at (" << i << "," << j << ")\n";
	    matrix[i][j] = num;
	}
    }
}

Matrix Matrix::naiveMultiply(Matrix &otherMatrix){
    // iterates through the rows first to multiply the matrix
    Matrix ret("Result matrix", size);
    for(unsigned long int i = 0; i < size; i++){
	for(unsigned long int j = 0; j < size; j++){
	    for(unsigned long int k = 0; k < size; k++){
		ret.matrix[i][j] += matrix[i][k] * otherMatrix.matrix[k][j];
	    }
	}
    }
    return ret;
}

// assumes the matrices have size 2^m
// current implementation is space inefficient
// future versions will avoid using temporary matrix objects
Matrix Matrix::strassenMultiply(Matrix &otherMatrix){
    if(size == 4){
	return naiveMultiply(otherMatrix);
    }else{
	Matrix A11 = getSubMatrix(1);
	Matrix A12 = getSubMatrix(2);
	Matrix A21 = getSubMatrix(3);
	Matrix A22 = getSubMatrix(4);

	Matrix B11 = otherMatrix.getSubMatrix(1);
	Matrix B12 = otherMatrix.getSubMatrix(2);
	Matrix B21 = otherMatrix.getSubMatrix(3);
	Matrix B22 = otherMatrix.getSubMatrix(4);

	Matrix temp = B11 + B22;
	Matrix I = (A11 + A22).strassenMultiply(temp);
	Matrix II = (A21 + A22).strassenMultiply(B11);
	temp = B12 - B22;
	Matrix III = A11.strassenMultiply(temp);
	temp = -B11 + B21;
	Matrix IV = A22.strassenMultiply(temp);
	Matrix V = (A11 + A12).strassenMultiply(B22);
	temp = B11 + B22;
	Matrix VI = (-A11 + A21).strassenMultiply(temp);
	temp = B21 + B22;
	Matrix VII = (A12 - A22).strassenMultiply(temp);

	Matrix C11 = I + IV - V + VII;
	Matrix C12 = III + V;
	Matrix C21 = II + IV;
	Matrix C22 = I + III - II + VI;

	Matrix ret("Result matrix", size);
	ret.joinMatrices(C11, C12, C21, C22);
	return ret;
    }
}

Matrix Matrix::operator-(){
    for(unsigned long int i = 0; i < size; i++){
	for(unsigned long int j = 0; j < size; j++){
	    matrix[i][j] = -matrix[i][j];
	}
    }
    return *this;
}

Matrix Matrix::operator+(const Matrix &n){
    Matrix ret("Result matrix", size);
    for(unsigned long int i = 0; i < size; i++){
	for(unsigned long int j = 0; j < size; j++){
	    ret.matrix[i][j] = matrix[i][j] + n.matrix[i][j];
	}	    
    }
    return ret;
}

Matrix Matrix::operator-(const Matrix &n){
    Matrix ret("Result matrix", size);
    for(unsigned long int i = 0; i < size; i++){
	for(unsigned long int j = 0; j < size; j++){
	    ret.matrix[i][j] = matrix[i][j] - n.matrix[i][j];
	}
    }
    return ret;
}

Matrix Matrix::operator+=(const Matrix &n){
    for(unsigned long int i = 0; i < size; i++){
	for(unsigned long int j = 0; j < size; j++){
	    matrix[i][j] += n.matrix[i][j];
	}
    } 
    return *this;
}

Matrix Matrix::operator-=(const Matrix &n){
    for(unsigned long int i = 0; i < size; i++){
	for(unsigned long int j = 0; j < size; j++){
	    matrix[i][j] -= n.matrix[i][j];
	}
    }
    return *this;
}

bool Matrix::operator==(const Matrix &n){
    for(unsigned long int i = 0; i < size; i++){
	for(unsigned long int j = 0; j < size; j++){
	    if(matrix[i][j] != n.matrix[i][j]){
		return false;
	    }
	}
    }
    return true;
}

bool Matrix::operator!=(const Matrix &n){
    for(unsigned long int i = 0; i < size; i++){
	for(unsigned long int j = 0; j < size; j++){
	    if(matrix[i][j] == n.matrix[i][j]){
		return false;
	    }
	}
    }
    return true;
}
