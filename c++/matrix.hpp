#if !defined(__MATRIX_HPP)
#define __MATRIX_HPP

#include <iostream>
#include <string>
#include <vector>

class Matrix{
    friend std::ostream &operator<<(std::ostream &os, const Matrix &M){
	os << "matrix: " << M.name << "\n";
	os << "size: " << M.size << "\n";
	for(auto &row : M.matrix){
	    for(auto &num : row){
		os << num << " ";
	    }
	    os << std::endl;
	}
	return os;
    };

    
public:
    Matrix(std::string name);
    Matrix(std::string name, unsigned long int size);

    inline std::string getName() const {return name;}
    inline unsigned long int getSize() const {return size;}
    void setSize(unsigned long int n);
    void setNum(unsigned long int i, unsigned long int j, int num);
    int getNum(unsigned long int i, unsigned long int j) const;
    Matrix getSubMatrix(int quadrant);
    void joinMatrices(const Matrix &m1, const Matrix &m2, const Matrix &m3, const Matrix &m4);
    std::vector<std::vector<int> > getMatrix() const {return matrix;}
    Matrix naiveMultiply(Matrix &otherMatrix);
    Matrix strassenMultiply(Matrix &otherMatrix);

    Matrix operator-();
    Matrix operator+(const Matrix &n);
    Matrix operator-(const Matrix &n);
    Matrix operator+=(const Matrix &n);
    Matrix operator-=(const Matrix &n);
    bool operator==(const Matrix &n);
    bool operator!=(const Matrix &n);

protected:

private:
    unsigned long int size;
    std::string name;
    std::vector<std::vector<int> > matrix;
};

std::ostream &operator<<(std::ostream &os, const Matrix &M);

#endif
