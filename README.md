# README #

A comparison of different matrix multiplication algorithms, with special attention to Strassen's algorithm.

## Current Goals ##

- Adding OpenMP in C++ implementation for parallezation
- Implementation of Strassen in Haskell
